package server

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

const ShutdownSec = 5

type Module interface {
	RegisterHandler(router *mux.Router)
}

type HttpModuleSupport interface {
	AddModule(path string, handler http.Handler)
}

type Server interface {
	Start() error
	Stop()
}

type HttpServer struct {
	srv    *http.Server
	router *mux.Router
}

type Config interface {
	GetAddr() string
	GetReadTimeout() int64
	GetWriteTimeout() int64
}

func NewHttpServer(config Config) (*HttpServer, error) {
	router := mux.NewRouter()
	srv := &http.Server{
		Addr:         config.GetAddr(),
		Handler:      router,
		ReadTimeout:  time.Duration(config.GetReadTimeout()) * time.Second,
		WriteTimeout: time.Duration(config.GetWriteTimeout()) * time.Second,
	}

	return &HttpServer{
		srv:    srv,
		router: router,
	}, nil
}

func (s *HttpServer) Start() error {
	return s.srv.ListenAndServe()
}

func (s *HttpServer) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), ShutdownSec*time.Second)
	defer cancel()
	return s.srv.Shutdown(ctx)
}

func (s *HttpServer) AddModule(path string, mod Module) {
	log.Printf("Adding module to path %s\n", path)
	subrouter := s.router.PathPrefix(path).Subrouter()
	mod.RegisterHandler(subrouter)
}

func ErrorResponse(w http.ResponseWriter, message string) {
	ErrorResponseWithCode(w, message, http.StatusBadRequest)
}

func ErrorResponseWithCode(w http.ResponseWriter, message string, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if _, err := fmt.Fprintf(w, `{"message":"%s"}`, message); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func SuccessResponseJson(w http.ResponseWriter, val interface{}) {
	jsonResp, err := json.Marshal(val)
	if err != nil {
		ErrorResponse(w, err.Error())
		return
	}
	w.WriteHeader(200)
	w.Write(jsonResp)
}
