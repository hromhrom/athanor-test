package storage

import (
	"database/sql"
	"fmt"
	"path/filepath"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
)

type DatabaseConfig interface {
	GetDsn() string
}

type Storage interface {
	Db() *sql.DB
	Migrate(path string) error
	Close() error
}

func NewStorage(config DatabaseConfig) (Storage, error) {
	return NewPgSqlStorage(config.GetDsn())
}

type PgSqlStorage struct {
	db *sql.DB
}

func NewPgSqlStorage(dsn string) (*PgSqlStorage, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	return &PgSqlStorage{
		db: db,
	}, nil
}

func (s *PgSqlStorage) Db() *sql.DB {
	return s.db
}

func (s *PgSqlStorage) Migrate(path string) error {
	driver, err := postgres.WithInstance(s.db, &postgres.Config{})
	if err != nil {
		return err
	}

	migratePath, err := filepath.Abs(path)
	if err != nil {
		return fmt.Errorf("Migration path not resolved: %s", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file://%s", migratePath),
		"postgres", driver)
	if err != nil {
		return err
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}
	return nil
}

func (s *PgSqlStorage) Close() error {
	return s.db.Close()
}
