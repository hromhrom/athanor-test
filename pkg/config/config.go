package config

import (
	"github.com/spf13/viper"
)

type HttpConfig struct {
	Addr         string `mapstructure:"Addr"`
	ReadTimeout  int64  `mapstructure:"ReadTimeout"`
	WriteTimeout int64  `mapstructure:"WriteTimeout"`
}

func (c *HttpConfig) GetAddr() string {
	return c.Addr
}

func (c *HttpConfig) GetReadTimeout() int64 {
	return c.ReadTimeout
}

func (c *HttpConfig) GetWriteTimeout() int64 {
	return c.WriteTimeout
}

type DatabaseConfig struct {
	Dsn string `mapstructure:"Dsn"`
}

func (d *DatabaseConfig) GetDsn() string {
	return d.Dsn
}

type Config struct {
	Http *HttpConfig     `mapstructure:"Http"`
	Db   *DatabaseConfig `mapstructure:"Db"`
}

// NewConfig - load config from file path/filename
func NewConfig(path string, filename string) (*Config, error) {
	viper.SetConfigName(filename)
	viper.AddConfigPath(path)
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var conf Config
	if err := viper.Unmarshal(&conf); err != nil {
		return nil, err
	}

	return &conf, nil
}
