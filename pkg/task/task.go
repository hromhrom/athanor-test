package task

import (
	"context"
	"errors"
	"log"
	"sync"
	"sync/atomic"
	"time"
)

type Task func(val interface{})

type TaskExecutor interface {
	Ready() bool
	Shutdown() error
	Add(task Task, param interface{}) error
	GetActiveTasks() int32
}

const TaskExecutorStopTimeoutSec = 20
const TaskExecutorStopRefreshTimeoutMillis = 1000

type taskExecutor struct {
	mu          *sync.Mutex
	activeTasks int32
	stopping    int32
}

func NewTaskExecutor() TaskExecutor {
	return &taskExecutor{
		mu:          &sync.Mutex{},
		activeTasks: 0,
		stopping:    0,
	}
}

func (te *taskExecutor) Add(task Task, param interface{}) error {
	if !te.Ready() {
		return errors.New("Task executor not ready for new tasks")
	}
	te.incActiveTasks()
	go func(param interface{}) {
		task(param)
		te.decrActiveTasks()
	}(param)
	return nil
}

func (te *taskExecutor) incActiveTasks() {
	te.mu.Lock()
	te.activeTasks++
	log.Printf("Active tasks count %d", te.activeTasks)
	te.mu.Unlock()
}

func (te *taskExecutor) decrActiveTasks() {
	te.mu.Lock()
	te.activeTasks--
	log.Printf("Active tasks count %d", te.activeTasks)
	te.mu.Unlock()
}

func (te *taskExecutor) GetActiveTasks() int32 {
	te.mu.Lock()
	defer te.mu.Unlock()
	return te.activeTasks
}

func (te *taskExecutor) Ready() bool {
	return te.stopping == 0
}

func (te *taskExecutor) Shutdown() error {
	atomic.StoreInt32(&te.stopping, 1)
	if te.GetActiveTasks() == 0 {
		return nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), TaskExecutorStopTimeoutSec*time.Second)
	defer cancel()
	ticker := time.NewTicker(TaskExecutorStopRefreshTimeoutMillis * time.Millisecond)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			log.Printf("Stop task executor by timeout\n")
			return ctx.Err()
		case <-ticker.C:
			at := te.GetActiveTasks()
			if at == 0 {
				return nil
			}
			log.Printf("Has %d active tasks\n", at)
		}
	}
}
