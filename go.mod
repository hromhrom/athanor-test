module bitbucket.org/hromhrom/athanor-test

go 1.13

require (
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.6.2
	github.com/gorilla/mux v1.7.1
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mattes/migrate v3.0.1+incompatible
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.4.0
	golang.org/x/sys v0.0.0-20191003212358-c178f38b412c // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
