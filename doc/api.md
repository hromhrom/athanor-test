
# Athanor test



## Indices

* [Default](#default)

  * [Retrieve Sha256 hash by task {id}](#1-retrieve-sha256-hash-by-task-{id})
  * [Sha256 task creation](#2- sha256-task-creation)


--------


## Default



### 1. Retrieve Sha256 hash by task {id}


Получение SHA-256 хеша по {id} задания.

Ответ:
* id - идентификатор созданного задания на вычисление хэша
* payload -  исходный текст
* hash_rounds_cnt - количество раз, которые берется хэш
* status - статус задания. Возможные значения: "in progress" - в обработке, "finished" - обработка завершена
* hash - результат хэширования


***Endpoint:***

```bash
Method: GET
Type: 
URL: {{SERVER}}/sha2/1
```



***Responses:***


Status: Retrieve Sha256 hash by task {id} | Code: 200



```js
{
    "id": 33,
    "payload": "test",
    "hash_rounds_cnt": 5,
    "status": "finished",
    "hash": "d32b3b15471a3ddfa23c5d6d147958e8e817f65878f3df30436e61fa639127b1"
}
```



### 2. Sha256 task creation


Запрос на вычисление нового SHA-256 хеша.

Параметры:
* payload - строка, для которой необходимо посчитать хэш
* hash_rounds_cnt - количество раз, которые берется хэш

Ответ:
* id - идентификатор созданного задания на вычисление хэша


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: {{SERVER}}/sha2
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Body:***

```js        
{
	"payload": "test",
	"hash_rounds_cnt": 5
}
```



***Responses:***


Status:  Sha256 task creation | Code: 200



```js
{
    "id": 34
}
```



---
[Back to top](#athanor-test)
> Made with &#9829; by [thedevsaddam](https://github.com/thedevsaddam) | Generated at: 2019-10-07 10:08:05 by [docgen](https://github.com/thedevsaddam/docgen)
