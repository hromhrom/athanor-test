package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/hromhrom/athanor-test/pkg/task"

	"bitbucket.org/hromhrom/athanor-test/internal/app/sha2"

	"bitbucket.org/hromhrom/athanor-test/pkg/server"

	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"

	"bitbucket.org/hromhrom/athanor-test/pkg/config"
	"bitbucket.org/hromhrom/athanor-test/pkg/storage"
)

func main() {
	var autoMigrate = flag.Bool("autoMigrate", false, "Run migrations on start")
	var configPath = flag.String("configPath", "./config", "path to config files")
	var configFile = flag.String("configFile", "config", "filename of config")
	var migrationsPath = flag.String("migrationsPath", "./migrations", "path to migrations")
	flag.Parse()

	done, errCh := start(*configPath, *configFile, *autoMigrate, *migrationsPath)

	select {
	case <-done:
		log.Println("Server stopped successfully")
	case err := <-errCh:
		log.Fatalf(err.Error())
	}
}

func start(configPath string, configFile string, autoMigrate bool, migrationsPath string) (<-chan struct{}, <-chan error) {
	errCh := make(chan error)
	done := make(chan struct{})
	go func() {
		config, err := config.NewConfig(configPath, configFile)
		if err != nil {
			errCh <- fmt.Errorf("Error reading config file %s", err)
		}

		storage, err := storage.NewStorage(config.Db)
		if err != nil {
			errCh <- fmt.Errorf("Error initializing db %s", err)
			return
		}

		if autoMigrate {
			if err := storage.Migrate(migrationsPath); err != nil {
				errCh <- fmt.Errorf("Error running migrations %s", err)
				return
			}
		}

		server, err := server.NewHttpServer(config.Http)
		if err != nil {
			errCh <- fmt.Errorf("Error staring http server %s", err)
			return
		}

		sha2Rep, err := sha2.NewSha2PgSqlRepository(storage.Db())
		if err != nil {
			errCh <- fmt.Errorf("Error creating sha2 repository %s", err)
			return
		}

		taskExecutor := task.NewTaskExecutor()
		sha2Serv := sha2.NewSha2Service(sha2Rep, taskExecutor)
		go func() {
			log.Println("Calculating unprocessed tasks")
			sha2Serv.CalcUnprocessed()
		}()
		sha2Module := sha2.NewSha2Module(sha2Serv)
		server.AddModule("/sha2", sha2Module)

		go func() {
			errCh <- server.Start()
			return
		}()

		// Setting up signal capturing
		signs := make(chan os.Signal, 1)
		signal.Notify(signs, syscall.SIGINT, syscall.SIGTERM)

		// Goroutine for stopping services
		go func() {
			sig := <-signs
			log.Printf("Signal %d received\n", sig)
			log.Printf("Stopping task executor active tasks %d\n", taskExecutor.GetActiveTasks())
			if err := taskExecutor.Shutdown(); err != nil {
				errCh <- err
				return
			}
			log.Println("Stopping web server")
			if err := server.Stop(); err != nil {
				errCh <- err
				return
			}
			log.Println("Closing storage")
			if err := storage.Close(); err != nil {
				errCh <- err
				return
			}
			done <- struct{}{}
		}()
	}()

	return done, errCh
}
