CREATE TABLE IF NOT EXISTS sha2(
   id serial PRIMARY KEY,
   payload TEXT NOT NULL,
   "hash" TEXT,
   hash_rounds_cnt INT NOT NULL,
   status SMALLINT NOT NULL
);