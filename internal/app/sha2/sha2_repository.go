package sha2

import (
	"database/sql"
)

type Sha2Repository interface {
	ByID(id int64) (*Sha2, error)
	EachNotProcessed(callback func(rec *Sha2)) error
	Create(payload string, hashRounds int32) (*Sha2, error)
	SetHash(id int64, hash string) error
}

type Sha2PgSqlRepository struct {
	db *sql.DB
}

func NewSha2PgSqlRepository(db *sql.DB) (Sha2Repository, error) {
	return &Sha2PgSqlRepository{
		db: db,
	}, nil
}

func (r *Sha2PgSqlRepository) ByID(id int64) (*Sha2, error) {
	stmt := "SELECT id, payload, hash, hash_rounds_cnt, status FROM sha2 where id = $1"
	record := Sha2{}
	if err := r.db.QueryRow(stmt, id).Scan(
		&record.ID,
		&record.Payload,
		&record.Hash,
		&record.HashRounds,
		&record.Status,
	); err != nil {
		// If not found return null
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &record, nil
}

func (r *Sha2PgSqlRepository) Create(payload string, hashRounds int32) (*Sha2, error) {
	insertStmt := "INSERT INTO sha2 (payload, hash_rounds_cnt, status) values ($1, $2, $3) RETURNING id"
	var id int64
	if err := r.db.QueryRow(insertStmt, payload, hashRounds, StatusInProgress).Scan(&id); err != nil {
		return nil, err
	}
	return r.ByID(id)
}

func (r *Sha2PgSqlRepository) SetHash(id int64, hash string) error {
	updateStmt := "UPDATE sha2 SET hash = $1, status = $2 where id = $3"
	if _, err := r.db.Exec(updateStmt, hash, StatusFinished, id); err != nil {
		return err
	}
	return nil
}

func (r *Sha2PgSqlRepository) EachNotProcessed(callback func(rec *Sha2)) error {
	stmt := "SELECT id, payload, hash, hash_rounds_cnt, status FROM sha2 WHERE status = $1 order by id"
	rows, err := r.db.Query(stmt, StatusInProgress)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		record := Sha2{}
		if err := rows.Scan(
			&record.ID,
			&record.Payload,
			&record.Hash,
			&record.HashRounds,
			&record.Status,
		); err != nil {
			return err
		}

		callback(&record)
	}
	return nil
}
