package sha2

const (
	StatusInProgress = 1
	StatusFinished   = 2
)

type Sha2 struct {
	ID         int64
	Status     int
	HashRounds int32
	Payload    string
	Hash       *string
}
