package sha2

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"log"

	"bitbucket.org/hromhrom/athanor-test/pkg/task"
)

type Sha2Service interface {
	Create(payload string, hashRounds int32) (*Sha2, error)
	Calc(rec *Sha2) error
	CalcUnprocessed() error
	Get(id int64) (*Sha2, error)
	calcHash(source string, hashRounds int32) (*string, error)
}

type sha2Service struct {
	repository   Sha2Repository
	taskExecutor task.TaskExecutor
}

func NewSha2Service(rep Sha2Repository, te task.TaskExecutor) Sha2Service {
	return &sha2Service{
		repository:   rep,
		taskExecutor: te,
	}
}

func (s *sha2Service) Create(payload string, hashRounds int32) (*Sha2, error) {
	record, err := s.repository.Create(payload, hashRounds)
	if err != nil {
		return nil, err
	}

	if err := s.Calc(record); err != nil {
		return nil, err
	}
	return record, nil
}

func (s *sha2Service) Get(id int64) (*Sha2, error) {
	return s.repository.ByID(id)
}

func (s *sha2Service) calcHash(src string, rounds int32) (*string, error) {
	if rounds <= 0 {
		return nil, errors.New("Hash rounds <= 0")
	}
	var res string = src
	var i int32 = 0
	for {
		hash := sha256.Sum256([]byte(res))
		log.Printf("SHA256(%s) = %x\n", res, hash)
		res = fmt.Sprintf("%x", hash)
		i++
		if i >= rounds {
			break
		}
	}
	return &res, nil
}

func (s *sha2Service) Calc(sh *Sha2) error {
	return s.taskExecutor.Add(func(val interface{}) {
		var rec *Sha2
		var ok bool
		if rec, ok = val.(*Sha2); !ok {
			log.Printf("Param is not Sha2 type")
		}
		hash, err := s.calcHash(rec.Payload, rec.HashRounds)
		if err != nil {
			log.Printf("Error during hash calculation: %s", err)
		}
		if err := s.repository.SetHash(rec.ID, *hash); err != nil {
			log.Printf("Error saving hash result: %s", err)
		}
	}, sh)
}

func (s *sha2Service) CalcUnprocessed() error {
	return s.repository.EachNotProcessed(func(rec *Sha2) {
		s.Calc(rec)
	})
}
