package sha2

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/hromhrom/athanor-test/pkg/server"
	"github.com/gorilla/mux"
	"gopkg.in/go-playground/validator.v9"
)

type Sha2CreateParams struct {
	Payload    *string `json:"payload" validate:"required,gt=0"`
	HashRounds *int32  `json:"hash_rounds_cnt" validate:"required,min=1,max=100"`
}

type Sha2CreateResponse struct {
	ID int64 `json:"id"`
}

type Sha2Response struct {
	ID         int64   `json:"id"`
	Payload    string  `json:"payload"`
	HashRounds int32   `json:"hash_rounds_cnt"`
	Status     string  `json:"status"`
	Hash       *string `json:"hash"`
}

func NewSha2Response(rec *Sha2) *Sha2Response {
	resp := &Sha2Response{
		ID:         rec.ID,
		Payload:    rec.Payload,
		HashRounds: rec.HashRounds,
		Hash:       rec.Hash,
	}
	resp.Status = resp.StatusLabel(rec.Status)
	return resp
}

func (s *Sha2Response) StatusLabel(statusID int) string {
	switch statusID {
	case StatusInProgress:
		return "in progress"
	case StatusFinished:
		return "finished"
	default:
		return "unknown"
	}
}

type Sha2Module struct {
	service Sha2Service
}

func NewSha2Module(s Sha2Service) *Sha2Module {
	return &Sha2Module{
		service: s,
	}
}

func (m *Sha2Module) RegisterHandler(router *mux.Router) {
	router.HandleFunc("", m.createHandler).
		Methods("POST")
	router.HandleFunc("/{id:[0-9]+}", m.viewHandler).
		Methods("GET")
}

func (m *Sha2Module) createHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	decoder := json.NewDecoder(r.Body)
	crp := Sha2CreateParams{}
	if err := decoder.Decode(&crp); err != nil {
		server.ErrorResponse(w, fmt.Sprintf("decode error: %s", err.Error()))
		return
	}

	val := validator.New()
	if err := val.Struct(&crp); err != nil {
		server.ErrorResponse(w, err.Error())
		return
	}

	var sha2 *Sha2
	var err error
	if sha2, err = m.service.Create(*crp.Payload, *crp.HashRounds); err != nil {
		server.ErrorResponse(w, err.Error())
		return
	}

	server.SuccessResponseJson(w, Sha2CreateResponse{
		ID: sha2.ID,
	})
}

func (m *Sha2Module) viewHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(r)
	var idParam string
	var ok bool
	if idParam, ok = vars["id"]; !ok {
		server.ErrorResponse(w, "Required query param {id} not set")
		return
	}
	id, err := strconv.ParseInt(idParam, 10, 64)
	if err != nil {
		server.ErrorResponse(w, "Query param {id} of wrong type")
		return
	}

	rec, err := m.service.Get(id)
	if err != nil {
		server.ErrorResponse(w, err.Error())
		return
	}

	if rec == nil {
		server.ErrorResponse(w, fmt.Sprintf("Task not found by id {%d}", id))
		return
	}

	server.SuccessResponseJson(w, NewSha2Response(rec))
}
