# Athanor test

Тестовое задания для Athanor

[Документация по API](doc/api.md)

## Установка

```bash
git clone https://hromhrom@bitbucket.org/hromhrom/athanor-test.git
docker-compose build
docker-compose up
```

## Запуск
```
./main -h
Usage of ./main:
  -autoMigrate
        Run migrations on start
  -configFile string
        filename of config (default "config")
  -configPath string
        path to config files (default "./config")
  -migrationsPath string
        path to migrations (default "./migrations")
```

После запуска сервер будет доступен по http://localhost:18080, см документацию на API выше

## Структура проекта
```
├── Athanor\ test.postman_collection.json
├── Dockerfile
├── README.md
├── cmd
│   └── app
│       └── main.go # точка входа в приложение
├── config
│   └── config.yaml # конфигурационный файл
├── doc
├── docker-compose.yaml
├── go.mod
├── go.sum
├── internal
│   └── app
│       └── sha2 # бизнес логика приложения
│           ├── models.go
│           ├── module.go
│           ├── sha2_repository.go
│           └── sha2_service.go
├── main
├── migrations # миграции для БД
│   ├── 000001_create_sha2_table.down.sql
│   └── 000001_create_sha2_table.up.sql
└── pkg
    ├── config # парсер конфигурации
    │   └── config.go
    ├── server # компонент сервера
    │   └── server.go
    ├── storage # компонент взаимодействия с БД
    │   └── storage.go
    └── task # компонент для выполнения отложенных заданий
        └── task.go
```

## Архитектура

При создании сервиса старался придерживаться чистой архитектуры. Зависимости между компонентами инъектятся в главном исполняемом файле cmd/app.main.go. Внутри между компонентами зависимостей уже нет.

### Общие компоненты
* Config - для парсинга конфигов и предоставления данных другим компонентам
* Server - сам веб-сервер. Позволяет добавлять в него отдельные модули.
* Storage - предоставляет доступ к БД. 
* TaskExecutor - выполняет отложенные задания. Подсчитывает число активных заданий, позволяет
завершать задания корректно во время остановки сервиса

### Domain logic
Логика сервиса находится в пакете internal/app/sha2. Разделена на слои:
* Бизнес-логика - выделена в Sha2Service
* Доступ к данным - в Sha2Repository




